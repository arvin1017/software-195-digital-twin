package com.software.digitaltwin.controller;

import com.software.digitaltwin.tcp.TCPUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StartBelt {

    @GetMapping("/startBelt")
    @ResponseBody
    public String startBelt(byte num, byte speed){
        boolean success = TCPUtils.startBelt(num, speed);
        return success ? "{\"status\": 0}" : "{\"status\": 1}";
    }
}
