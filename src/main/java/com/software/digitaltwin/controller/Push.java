package com.software.digitaltwin.controller;

import com.software.digitaltwin.tcp.TCPUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Push {

    @GetMapping("/push")
    @ResponseBody
    public String startBelt(byte num, byte hold){
        boolean success = TCPUtils.push(num, hold);
        return success ? "{\"status\": 0}" : "{\"status\": 1}";
    }
}
